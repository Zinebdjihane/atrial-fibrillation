import math
import keras
import wfdb
from biosppy.signals import ecg
from sklearn.metrics import confusion_matrix, accuracy_score
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
import pandas as pd
import scipy.io as sio
from os import listdir
from os.path import isfile, join
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, Conv2D, MaxPooling2D, Flatten, LSTM, Conv1D, \
    GlobalAveragePooling1D, MaxPooling1D, MaxPool1D, BatchNormalization, UpSampling1D
from sklearn.metrics import classification_report
from keras import regularizers


import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

np.random.seed(7)


number_of_classes = 2  # Total number of classes
cross_val = False

def change(x):  # From boolean arrays to decimal arrays
    answer = np.zeros((np.shape(x)[0]))
    for i in range(np.shape(x)[0]):
        max_value = max(x[i, :])
        max_index = list(x[i, :]).index(max_value)
        answer[i] = max_index
    return answer.astype(np.int)


mypath = 'mitseg/'  # Training directory

#mypath2='pad/1.0.0/'
onlyfiles = [f for f in listdir(mypath) if (isfile(join(mypath, f)) and f[0] == '2')]

bats = [f for f in onlyfiles if f.endswith('.mat') ]

check = 100
mats = [f for f in bats if (np.shape(sio.loadmat(mypath + f)['strips_rhythm'])[1] >= check)]
"""
onlyfiles2 = [f for f in listdir(mypath) if (isfile(join(mypath, f)) and f[1] == 'z')]

bats2 = [f for f in onlyfiles2 if f.endswith('.mat') ]
mats2 = [f for f in bats2 if (np.shape(sio.loadmat(mypath + f)['strips_rhythm'].T)[1] >= check)]




onlyfiles3 = [f for f in os.listdir(mypath) if (isfile(join(mypath, f)) and f[0] == 'n' or f[0] == 'p'  )]

bats3 = [f for f in onlyfiles3 if f.endswith('.dat') ]


bats3 = [x.replace('.dat', '') for x in bats3]

bats3= list( dict.fromkeys(bats3) )
"""
mats3 = mats
print(mats3)
print(len(mats3))
big = 9000
"""
csv_fn = "data.csv"
def filterSignalBios(data, sampling_rate=300.0):

    out = ecg.ecg(data, sampling_rate=sampling_rate, show=False)

    return out[1]

file = pd.read_csv(csv_fn)
result = file.values
tryu=[]
count=1

for i in result:
    tryu.append(i[:])
    if count ==8528:
        break
    count+=1
size = len(mats3)
"""
size = len(mats3)
X = np.zeros((size, big))
X1 = np.zeros((size, big))
for i in range(size):
    dummy = sio.loadmat(mypath + mats3[i])

    #tryu[i] = np.array(tryu[i])

    if ( 'strips_rhythm' in dummy.keys() ):
            dummy = dummy['strips_rhythm'][0, :]
            print(dummy)
            dummy = np.nan_to_num(dummy)
    """"
        elif ( 'strips_rhythm' in dummy.keys() ):
            dummy = (dummy['strips_rhythm'].T)[0,:]
            dummy = np.nan_to_num(dummy)
    elif mats3[i][0] == 'N':
        dummy = wfdb.io.rdsamp(mypath+mats3[i])
        dummy = np.nan_to_num(dummy)

        dummy= (dummy[0].T)[1]
    """
    if (big - len(dummy)) <= 0:
        X[i, :] = dummy[0:big]

    else:
        b = dummy[0:(big - len(dummy))]

        goal = np.hstack((dummy, b))

        while len(goal) != big:
            b = dummy[0:(big - len(goal))]
            goal = np.hstack((goal, b))
        X[i, :] = goal
    #X1[i, :] = np.concatenate((X[i],tryu[i]), axis=0)
X1= X
#big = 10100

""""
onlyfiles3 = [f for f in os.listdir(mypath2) if (isfile(join(mypath2, f)) and f[0] == 'N'  )]

bats3 = [f for f in onlyfiles3 if f.endswith('.dat')  ]


bats3 = [x.replace('.dat', '') for x in bats3]
bats3= list( dict.fromkeys(bats3) )
mats3 = mats + mats2 + bats3
print(len(mats3))
"""
target_train = np.zeros((size, 1))
Train_data = pd.read_csv(mypath + 'REFERENCE.csv', sep=',', header=None, names=None)
for i in range(size):
    if Train_data[1][i] == 'N':
        target_train[i] = 0
    elif Train_data[1][i] == 'A':
        target_train[i] = 1
    """
    elif Train_data[1][i] == 'O':
        target_train[i] = 2

       
    else:
        target_train[i] = 3
    """

for i in target_train:
    print(i)
Label_set = np.zeros((size, number_of_classes))
for i in range(size):
    dummy = np.zeros((number_of_classes))
    dummy[int(target_train[i])] = 1
    Label_set[i, :] = dummy

X1 = (X1 - X1.mean())/(X1.std()) #Some normalization here
X1 = np.expand_dims(X1, axis=2) #For Keras's data input size

values = [i for i in range(size)]
permutations = np.random.permutation(values)
X1 = X1[permutations, :]
Label_set = Label_set[permutations, :]

train = 0.8 #Size of training set in percentage
X_train = X1[:int(train * size), :]
Y_train = Label_set[:int(train * size), :]
X_val = X1[int(train * size):, :]
Y_val = Label_set[int(train * size):, :]

# def train_and_evaluate__model(model, X_train, Y_train, X_val, Y_val, i):
# def create_model():
model = Sequential()
model.add(Conv1D(32, 5, activation='relu', input_shape=(big, 1)))

model.add(MaxPooling1D(2))

#model.add(Dropout(0.25))
model.add(Conv1D(32, 5, activation='relu'))

model.add(MaxPooling1D(2))

#model.add(Dropout(0.5))
model.add(Conv1D(64, 5, activation='relu'))

model.add(MaxPooling1D(2))

#model.add(Dropout(0.25))

#model.add(Dense(16, kernel_initializer='normal', activation='relu'))


model.add(Conv1D(64, 5, activation='relu'))

model.add(MaxPooling1D(2))
#model.add(Dropout(0.25))

model.add(Conv1D(128, 5, activation='relu'))

model.add(MaxPooling1D(2))

model.add(Conv1D(128, 5, activation='relu'))

model.add(MaxPooling1D(2))

model.add(Dropout(0.5))
model.add(Conv1D(256, 5, activation='relu'))

model.add(MaxPooling1D(2))

model.add(Conv1D(256, 5, activation='relu'))

model.add(MaxPooling1D(2))

model.add(Dropout(0.5))
model.add(Conv1D(512, 5, activation='relu'))

model.add(MaxPooling1D(2))

model.add(Dropout(0.5))
model.add(Conv1D(512, 5, activation='relu'))

model.add(Flatten())

model.add(Dense(128, kernel_initializer='normal', activation='relu'))

model.add(Dropout(0.5))
model.add(Dense(32, kernel_initializer='normal', activation='relu'))
#model.add(Dropout(0.5))
##model.add(Dense(64, kernel_initializer='normal', activation='relu'))
#model.add(Dropout(0.5))
model.add(Dense(number_of_classes, kernel_initializer='normal', activation='softmax'))
sgd=keras.optimizers.Adam(learning_rate=0.0001, beta_1=0.9, beta_2=0.999, amsgrad=False)
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
checkpointer = ModelCheckpoint(filepath='conv_models/Best_model of '  + '.h5', monitor='val_acc',
                                   verbose=1, save_best_only=True)

hist = model.fit(X_train, Y_train, epochs=20, batch_size=30, validation_data=(X_val, Y_val), verbose=2,
                     shuffle=True, callbacks=[checkpointer])

predictions = model.predict(X_val)
score = accuracy_score(change(Y_val), change(predictions))
print(score)
results = confusion_matrix(change(Y_val), change(predictions))
print ('Confusion Matrix :')
print(results)
# skf = StratifiedKFold(n_splits=2,shuffle=True)
target_names = ['class 0', 'class 1']
print(classification_report(change(Y_val), change(predictions)),target_names)




