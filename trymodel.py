from sklearn.metrics import confusion_matrix, accuracy_score
from keras.callbacks import ModelCheckpoint
from biosppy.signals import ecg
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import MinMaxScaler, RobustScaler
import pandas as pd
import scipy.io as sio
from matplotlib import pyplot as plt
from os import listdir
from os.path import isfile, join
import numpy as np
import seaborn as sns
import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, Conv2D, MaxPooling2D, Flatten, LSTM, Conv1D, \
    GlobalAveragePooling1D, MaxPooling1D, MaxPool1D
from sklearn.metrics import classification_report
from keras import regularizers
#import wave
#import getFeatures

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

np.random.seed(7)


number_of_classes = 4  # Total number of classes
cross_val = False

def change(x):  # From boolean arrays to decimal arrays
    answer = np.zeros((np.shape(x)[0]))
    for i in range(np.shape(x)[0]):
        max_value = max(x[i, :])
        max_index = list(x[i, :]).index(max_value)
        answer[i] = max_index
    return answer.astype(np.int)


mypath = 'training2017/'  # Training directory
onlyfiles = [f for f in listdir(mypath) if (isfile(join(mypath, f)) and f[1] == '0')]

bats = [f for f in onlyfiles if f.endswith('.mat') ]

check = 100
mats = [f for f in bats if (np.shape(sio.loadmat(mypath + f)['val'])[1] >= check)]

onlyfiles2 = [f for f in listdir(mypath) if (isfile(join(mypath, f)) and f[1] == 'z')]

bats2 = [f for f in onlyfiles2 if f.endswith('.mat') ]
mats2 = [f for f in bats2 if (np.shape(sio.loadmat(mypath + f)['strips_rhythm'].T)[1] >= check)]

mats3 = mats + mats2

big = 10036

csv_fn = "download(21).csv"

file = pd.read_csv(csv_fn)
result = file.values
tryu=[]
count=1

for i in result:
    tryu.append(i[2:])

size = len(tryu)

X = np.zeros((size, big))
X1 = np.zeros((size, big+64))
for i in range(size):
    tryu[i] = np.array(tryu[i])

    dummy = sio.loadmat(mypath + mats3[i])

    if ( 'val' in dummy.keys() ):
        dummy = dummy['val'][0, :]

    else:
        dummy = dummy['strips_rhythm'][0, :]
    if (big - len(dummy)) <= 0:
        X[i, :] = dummy[0:big]

    else:
        b = dummy[0:(big - len(dummy))]

        goal = np.hstack((dummy, b))

        while len(goal) != big:
            b = dummy[0:(big - len(goal))]
            goal = np.hstack((goal, b))
        X[i, :] = goal
    X1[i, :] = np.concatenate((tryu[i], X[i]), axis=0)

big = 10100


target_train = np.zeros((size, 1))
Train_data = pd.read_csv(mypath + 'REFERENCE.csv', sep=',', header=None, names=None)
for i in range(size):
    if Train_data.loc[Train_data[0] == mats3[i][:6], 1].values == 'N':
        target_train[i] = 0
    elif Train_data.loc[Train_data[0] == mats3[i][:6], 1].values == 'A':
        target_train[i] = 1
    elif Train_data.loc[Train_data[0] == mats3[i][:6], 1].values == 'O':
        target_train[i] = 2
    else:
        target_train[i] = 3

Label_set = np.zeros((size, number_of_classes))
for i in range(size):
    dummy = np.zeros((number_of_classes))
    dummy[int(target_train[i])] = 1
    Label_set[i, :] = dummy

X1 = (X1 - X1.mean())/(X1.std()) #Some normalization here
X1 = np.expand_dims(X1, axis=2) #For Keras's data input size

Label_set = Label_set[:size, :]


# def train_and_evaluate__model(model, X_train, Y_train, X_val, Y_val, i):
# def create_model():
def create_model():
    # def create_model():
    model = Sequential()
    model.add(Conv1D(128, 55, activation='relu', input_shape=(big, 1)))
    model.add(MaxPooling1D(10))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 25, activation='relu'))
    model.add(MaxPooling1D(5))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 10, activation='relu'))
    model.add(MaxPooling1D(5))
    model.add(Dropout(0.5))
    model.add(Conv1D(128, 5, activation='relu'))
    model.add(GlobalAveragePooling1D())
    # model.add(Flatten())
    model.add(Dense(256, kernel_initializer='normal', activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(128, kernel_initializer='normal', activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, kernel_initializer='normal', activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(number_of_classes, kernel_initializer='normal', activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model


def train_and_evaluate__model(model, X_train, Y_train, X_val, Y_val, i):
    checkpointer = ModelCheckpoint(filepath='conv_models/2Best_model of ' + str(i) + '.h5', monitor='val_acc',
                                   verbose=1, save_best_only=True)

    hist = model.fit(X_train, Y_train, epochs=500, batch_size=256, validation_data=(X_val, Y_val), verbose=2,
                     shuffle=True, callbacks=[checkpointer])
    pd.DataFrame(hist.history).to_csv(path_or_buf='conv_models/2History ' + str(i) + '.csv')
    model.save('my_model ' + str(i) + '.h5')
    predictions = model.predict(X_val)
    score = accuracy_score(change(Y_val), change(predictions))
    print(score)
    df = pd.DataFrame(change(predictions))
    df.to_csv(path_or_buf='conv_models/2Preds_' + str(format(score, '.4f')) + '__' + str(i) + '.csv', index=None,
              header=None)
    model.save('conv_models/2' + str(format(score, '.4f')) + '__' + str(i) + '_my_model.h5')
    pd.DataFrame(confusion_matrix(change(Y_val), change(predictions))).to_csv(
        path_or_buf='conv_models/2Result_Conf' + str(format(score, '.4f')) + '__' + str(i) + '.csv', index=None,
        header=None)
    results = confusion_matrix(change(Y_val), change(predictions))

    print ('Confusion Matrix :')
    print(results)
    target_names = ['class 0', 'class 1', 'class 2','class 3']
    print (classification_report(change(Y_val), change(predictions)))

skf = StratifiedKFold(n_splits=5,shuffle=True)
target_train = target_train.reshape(size,)
# print(skf.get_n_splits(X, target_train))
# print(skf.split(X, target_train))
for i, (train_index, test_index) in enumerate(skf.split(X, target_train)):
	print("TRAIN:", train_index, "TEST:", test_index)
	# train = 0.9
	# print('Training_size is ', int(train*size))
	# print('Validation_size is ', size - int(train*size))
	X_train = X[train_index, :]
	Y_train = Label_set[train_index, :]
	X_val = X[test_index, :]
	Y_val = Label_set[test_index, :]
	# X_train = X[:int(train*size), :]
	# Y_train = Label_set[:int(train*size), :]
	# X_val = X[int(train*size):, :]
	# Y_val = Label_set[int(train*size):, :]
	# model = None
	model = create_model()
	train_and_evaluate__model(model, X_train, Y_train, X_val, Y_val, i)
# skf = StratifiedKFold(n_splits=2,shuffle=True)
# target_train = target_train.reshape(size,)

# for i, (train_index, test_index) in enumerate(skf.split(X, target_train)):
# print("TRAIN:", train_index, "TEST:", test_index)
# X_train = X[train_index, :]
# Y_train = Label_set[train_index, :]
# X_val = X[test_index, :]
# Y_val = Label_set[test_index, :]
# model = None
# model = create_model()
# train_and_evaluate__model(model, X_train, Y_train, X_val, Y_val, i)
