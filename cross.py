import math
import os
from os import listdir
from os.path import isfile, join

import numpy as np
import scipy.io as sio
import pandas as pd
import wfdb

def buffer(data, duration, dataOverlap):
    numberOfSegments = int(math.ceil((len(data) - dataOverlap) / (duration - dataOverlap)))
    # print(data.shape)
    tempBuf = [data[i:i + duration] for i in range(0, len(data), (duration - int(dataOverlap)))]
    tempBuf[numberOfSegments - 1] = np.pad(tempBuf[numberOfSegments - 1],
                                           (0, duration - tempBuf[numberOfSegments - 1].shape[0]), 'constant')
    tempBuf2 = np.vstack(tempBuf[0:numberOfSegments])
    return tempBuf2

mypath = 'a/'  # Training directory
onlyfiles = [f for f in listdir(mypath) if (isfile(join(mypath, f)) and f[1] == 'z')]

bats = [f for f in onlyfiles if f.endswith('.mat') ]

check = 100
mats = [f for f in bats if (np.shape(sio.loadmat(mypath + f)['strips_rhythm'].T)[1] >= check)]
""""
onlyfiles2 = [f for f in listdir(mypath) if (isfile(join(mypath, f)) and f[1] == 'z')]

bats2 = [f for f in onlyfiles2 if f.endswith('.mat') ]
mats2 = [f for f in bats2 if (np.shape(sio.loadmat(mypath + f)['strips_rhythm'].T)[1] >= check)]

onlyfiles3 = [f for f in os.listdir(mypath) if (isfile(join(mypath, f)) and f[0] == 'n' or f[0] == 'p'  )]

bats3 = [f for f in onlyfiles3 if f.endswith('.dat') ]


bats3 = [x.replace('.dat', '') for x in bats3]

bats3= list( dict.fromkeys(bats3) )
"""
mats3 = mats
mats3 = [x.replace('.mat', '') for x in mats3]
print(mats3)
print(len(mats3))
size = len(mats3)
sampleRate = 250
windowLen = 36
shiftLen = 15
duration = int(windowLen * sampleRate)
dataOverlap = (windowLen - shiftLen) * sampleRate

df = pd.read_csv('a/mitREF.csv')
saved_column = list(df.ann) #you can also use df['column_name']
print(saved_column)
annList=[]
nameList=[]
ann=0
count=200000
for i in range(size):

    if mats3[i][0] == 'A':
        # tryu[i] = np.array(tryu[i])
        dummy = sio.loadmat(mypath + mats3[i])
        if ('strips_rhythm' in dummy.keys()):

            dummy = dummy['strips_rhythm'].T[0, :]
            if len(dummy) > 9000:
                bufOut = buffer(dummy, duration, dataOverlap)
                for sa in bufOut:
                    sio.savemat('mitseg/' +  str(count) +'.mat',{'strips_rhythm':sa})
                    annList.append(saved_column[ann])
                    nameList.append(str(count))
                    count+=1
            else:

                sio.savemat('mitseg/' + str(count) + '.mat', {'strips_rhythm': dummy})
                annList.append(saved_column[ann])
                nameList.append(str(count))
                count += 1
            print(annList)
        ann+=1

import pandas as pd
L=zip(nameList, annList)
df = pd.DataFrame(L, columns=["name","ann"])
df.to_csv('mitseg\REFERENCE.csv', index=False)